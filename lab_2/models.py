from django.db import models
from django.db.models.fields import CharField

class Note(models.Model) :
    To = models.CharField(max_length=30)
    From = models.CharField(max_length=30)
    Title = models.CharField(max_length=100)
    Message = models.CharField(max_length=500)
