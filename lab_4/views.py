from django.shortcuts import redirect, render
from .forms import NoteForm
from lab_2.models import Note

# Create your views here.
def index(request) :
    note = Note.objects.all()
    response = {'note' : note}
    return render(request, 'lab4_index.html', response)

def add_note(request):
    context ={}
    # create object of form
    form = NoteForm(request.POST or None, request.FILES or None)
    if request.method == "POST":
        # check if form data is valid
        if form.is_valid():
            # save the form data to model
            form.save()
            return redirect('/lab-4/')
    context['form']= form
    return render(request, "lab4_form.html", context)

def note_list(request) :
    note = Note.objects.all()
    response = {'note' : note}
    return render(request, 'lab4_note_list.html', response)