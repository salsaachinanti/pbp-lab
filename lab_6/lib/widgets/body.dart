import 'package:flutter/material.dart';
import 'package:lab_6/constants.dart';

import 'daftar_layanan.dart';
import 'header_searchbox.dart';
import 'rekomendasi.dart';
import 'title_bbtn.dart';

class Body extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // It will provie us total height  and width of our screen
    Size size = MediaQuery.of(context).size;
    // it enable scrolling on small device
    return SingleChildScrollView(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          HeaderWithSearchBox(size: size),
          TitleWithMoreBtn(title: "Rekomendasi", press: () {}),
          RecomendsPlants(),
          TitleWithMoreBtn(title: "Daftar Layanan", press: () {}),
          DaftarLayanan(),
          SizedBox(height: kDefaultPadding),
        ],
      ),
    );
  }
}
