import 'package:date_field/date_field.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:lab_7/constants.dart';
import 'package:lab_7/widget/button_widget.dart';

Future main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await SystemChrome.setPreferredOrientations([
    DeviceOrientation.portraitUp,
    DeviceOrientation.portraitDown,
  ]);

  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  static final String title = 'Formulir Pendaftaran Relawan Donor Darah';

  @override
  Widget build(BuildContext context) => MaterialApp(
        debugShowCheckedModeBanner: false,
        title: title,
        theme: ThemeData(primaryColor: kPrimaryColor),
        home: MainPage(title: title),
      );
}

class MainPage extends StatefulWidget {
  final String title;

  const MainPage({
    @required this.title,
  });

  @override
  _MainPageState createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {
  final formKey = GlobalKey<FormState>();
  String nama = '';
  String nik = '';
  String sentra = '';
  String goldar = 'Pilih';

  @override
  Widget build(BuildContext context) => Scaffold(
        appBar: AppBar(
          title: Text(widget.title),
        ),
        body: Form(
          key: formKey,
          //autovalidateMode: AutovalidateMode.onUserInteraction,
          child: ListView(
            padding: EdgeInsets.all(16),
            children: [
              buildName(),
              const SizedBox(height: 32),
              buildTanggalLahir(),
              const SizedBox(height: 32),
              buildNIK(),
              const SizedBox(height: 32),
              buildSentra(),
              const SizedBox(height: 32),
              buildGoldar(),
              const SizedBox(height: 32),
              buildWaktuDonor(),
              const SizedBox(height: 32),
              buildSubmit(),
            ],
          ),
        ),
      );

  Widget buildName() => TextFormField(
        decoration: InputDecoration(
          hintText: 'Masukkan nama lengkap',
          labelText: 'Nama Lengkap',
          border: OutlineInputBorder(),
        ),
        validator: (value) {
          if (value.length < 4) {
            return 'Masukkan nama yang valid';
          } else {
            return null;
          }
        },
        maxLength: 30,
        onSaved: (value) => setState(() => nama = value),
      );

  Widget buildNIK() => TextFormField(
        decoration: InputDecoration(
          hintText: 'Masukkan NIK',
          labelText: 'Nomor Induk Kependudukan (NIK)',
          border: OutlineInputBorder(),
        ),
        validator: (value) {
          if (value.length < 16) {
            return 'Masukkan NIK yang valid';
          } else {
            return null;
          }
        },
        maxLength: 16,
        onSaved: (value) => setState(() => nik = value),
      );

  Widget buildSentra() => TextFormField(
        decoration: InputDecoration(
          hintText: 'Masukkan Sentra Layanan Donor Darah',
          labelText: 'Sentra Layanan Donor Darah',
          border: OutlineInputBorder(),
        ),
        validator: (value) {
          if (value == null || value.isEmpty) {
            return 'Masukkan Sentra Layanan yang valid';
          } else {
            return null;
          }
        },
        maxLength: 50,
        onSaved: (value) => setState(() => sentra = value),
      );

  Widget buildWaktuDonor() => DateTimeFormField(
        decoration: const InputDecoration(
          hintStyle: TextStyle(color: Colors.black45),
          errorStyle: TextStyle(color: Colors.redAccent),
          border: OutlineInputBorder(),
          suffixIcon: Icon(Icons.event_note),
          labelText: 'Waktu Donor Darah',
        ),
        firstDate: DateTime.now().add(const Duration(days: 1)),
        initialDate: DateTime.now().add(const Duration(days: 1)),
        validator: (DateTime e) {
          if (e == null) {
            return 'Masukkan waktu donor yang valid';
          } else {
            return null;
          }
        },
        onDateSelected: (DateTime value) {
          print(value);
        },
      );

  Widget buildTanggalLahir() => DateTimeFormField(
        decoration: const InputDecoration(
          hintStyle: TextStyle(color: Colors.black45),
          errorStyle: TextStyle(color: Colors.redAccent),
          border: OutlineInputBorder(),
          suffixIcon: Icon(Icons.event_note),
          labelText: 'Tanggal Lahir',
        ),
        firstDate: DateTime.now(),
        initialDate: DateTime.now(),
        validator: (DateTime e) {
          if (e == null) {
            return 'Masukkan tanggal lahir yang valid';
          } else {
            return null;
          }
        },
        onDateSelected: (DateTime value) {
          print(value);
        },
      );

  Widget buildGoldar() => DropdownButtonFormField(
        decoration: const InputDecoration(
            border: OutlineInputBorder(),
            labelText: 'Golongan Darah',
            hintText: 'Pilih Golongan Darah'),
        value: goldar,
        validator: (value) {
          if (value == 'Pilih') {
            return 'Pilih Golongan Darah';
          } else {
            return null;
          }
        },
        onChanged: (String newValue) {
          setState(() {
            goldar = newValue;
          });
        },
        items: <String>['Pilih', 'A', 'B', 'O', 'AB']
            .map<DropdownMenuItem<String>>((String value) {
          return DropdownMenuItem<String>(
            value: value,
            child: Text(value),
          );
        }).toList(),
      );

  Widget buildSubmit() => Builder(
        builder: (context) => ButtonWidget(
          text: 'Submit',
          onClicked: () {
            final isValid = formKey.currentState.validate();

            if (isValid) {
              formKey.currentState.save();

              final message = 'Pendaftaran berhasil!';
              final snackBar = SnackBar(
                content: Text(
                  message,
                  style: TextStyle(fontSize: 20),
                ),
                backgroundColor: Colors.green,
              );
              ScaffoldMessenger.of(context).showSnackBar(snackBar);
            }
          },
        ),
      );
}
