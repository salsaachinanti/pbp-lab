Nama : Salsabila Zahra Chinanti
NPM : 2006596762
Tugas Individu PBP Week 4

1. Apakah perbedaan antara JSON dan XML?
    Sebelum mengetahui perbedaan dari JSON dan XML, kita perlu mengetahui pengertian dari JSON dan XML. JSON adalah singkatan dari JavaScript object notation, yaitu format yang digunakan untuk menyimpan dan mentransfer data. Struktur data JSON sederhana dan mudah dipahami sehingga sering digunakan pada API.

    Sementara itu, XML atau Extensible Markup Language adalah bahasa yang berfungsi untuk menyederhanakan proses penyimpanan dan pengiriman data antarserver. XML akan menyimpan data dalam format teks yang sederhana, baik ketika sebelum ditransfer maupun setelah ditransfer ke server lain.

    Perbedaan :
    - Data JSON terdefinisi sementara data XML tidak terdefinisi
    - JSON hanya bisa menggunakan satu tipe encoding, yaitu UTF-8, sementara XML bisa menggunakan berbagai tipe encoding
    - JSON lebih sering menggunakan tipe data numerik dan teks sementara XML mendukung banyak tipe data, seperti angka, gambar, dan teks.
    - JSON menggunakan value atau pair key untuk menyusun data sehingga lebih mudah dibaca sementara XML menggunakan struktur teks
    - JSON dapat diparse menggunakan Javascript standar sementara XML harus diparse dengan parser
    - JSON memiliki tingkat sekuritas yang lebih rendah dari XML
    - JSON dapat diakses di lebih banyak web browser sementara XML hanya dapat diakses oleh browser tertentu

2. Apakah perbedaan antara HTML dan XML?

    HTML atau Hypertext Markup Language adalah bahasa yang biasa digunakan untuk membuat struktur halaman situs web. Menggunakan HTML, pengguna dapat membuat dan menyusun heading, paragraf, gambar, link, dan lain sebagainya melalui halaman situs web.

    Sementara itu, seperti yang telah disebutkan pada nomor 1, XML adalah Extensible Markup Language yang berfungsi untuk menyimpan dan mengirimkan data. Struktur XML yang sederhana terdiri dari tiga segmen, yaitu deklarasi, atribut, dan elemen. 

    Perbedaan :
    - Antarmuka browser XML lebih sulit dibuat daripada HTML
    - XML dapat digunakan untuk pertukaran data sementara HTML digunakan untuk penyajian data
    - HTML tidak terdapat pemeriksaan data dan struktur sementara XML perlu pemeriksaan data dan struktur
    - XML bersifat Case Sensitive sementara HTML bersifat Case Insensitive
    - Tag HTML terbatas sementara Tag XML dapat dikembangkan
    - HTML memiliki dukungan namespaces sementara XML tidak


Sumber :
https://id.sawakinome.com/articles/protocols--formats/difference-between-json-and-xml-3.html
https://blogs.masterweb.com/perbedaan-xml-dan-html/
https://www.niagahoster.co.id/blog

